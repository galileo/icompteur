<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
<?php

ini_set('display_errors', 0);

session_start();
date_default_timezone_set('Europe/Paris');
snmp_set_quick_print(1);

$arrayKM = array(100,101,102,103,104,105,106,107,108,110,111,112);

//emplacement
$oidEmplacement="iso.3.6.1.2.1.1.6.0";
//modèle
$oidModel="iso.3.6.1.2.1.1.1.0";
//Numéro de série
$oidSerial = "iso.3.6.1.2.1.43.5.1.1.17.1";
//Nom de l'imprimante
$oidNom="iso.3.6.1.2.1.1.5.0";
//Compteur d'impression de base
//$oidBase="iso.3.6.1.4.1.18334.1.1.1.5.7.2.1";
$oidBase="iso.3.6.1.4.1.18334.1.1.1.5.7.2";

/*
copies
	nb		.1.1
	couleur	.2.1
	mono	.3.1
	2c		.4.1
imp
	nb		.1.2
	couleur	.2.2
	mono	.3.2
	2c		.4.2
*/

//Compteur d'impression de total
$oidTotal=$oidBase.".1.1.0";
//Compteur d'impression détaillé
$oidDetail=$oidBase.".2.1.5";

$intTimeout=20000;

echo "<H1>COPIEURS KONICA-Minolta</H1> </BR><TABLE border='1'>";

echo "<col align='center'>";
echo "<col align='center'>";
echo "<col align='center'>";
echo "<col align='center'>";
echo "<col align='center'>";
echo "<col align='center'>";
echo "<col align='center'>";
echo "<col align='center'>";


echo "<BOLD><TR align='center'><TD></<TD><TD></<TD></TD><TD></TD>TD><TD></TD><TD></TD><TD></TD><TD colspan='4'>Copies</TD><TD colspan='4'>Impression</TD></TR>";

echo "<TR align='center'>";
echo "<TD>Adresse IP</TD><TD>Emplacement</TD><TD>Modèle </TD><TD>NOM</TD><TD>Numéro de série</TD>";
echo "<TD>Total</TD>";
echo "<TD>NB</TD><TD>Couleur</TD><TD>mono</TD><TD>2 Couleurs</TD><TD>NB</TD><TD>Couleur</TD><TD>mono</TD><TD>2 Couleurs</TD>";
echo "</TR></BOLD>";

foreach ($arrayKM as $KM) {
	$ip="10.72.8.".$KM;
	echo "<TR><TD>$ip</TD>";

        $chrEmplacement=snmpget("$ip", "public", "$oidEmplacement",$intTimeout);
        echo "<TD>$chrEmplacement</TD>";

	$chrNom=snmpget("$ip", "public", "$oidNom",$intTimeout);
	echo "<TD>$chrNom</TD>";

	$chrModel=snmpget("$ip", "public", "$oidModel",$intTimeout);
        echo "<TD>$chrModel</TD>";


	$chrSerial=snmpget("$ip", "public", "$oidSerial",$intTimeout);
        echo "<TD>$chrSerial</TD>";

	$intTotal=snmpget("$ip", "public", "$oidTotal",$intTimeout);
	echo "<TD>$intTotal</TD>";

	//$oidSerial="iso.3.6.1.2.1.43.5.1.1.17.1";
	//$intSerial=snmpget("$ip", "public", "$oidSerial",$intTimeout);
	//echo "<TD>$intSerial</TD>";

	//$oidBase="iso.3.6.1.4.1.2385.1.1.19.2.1.3.5.4";

	//$oidNb=$oidBase.".61";
	//$intNB=snmpget("$ip", "public", $oidNb,$intTimeout);
	//echo "<TD>$intNB</TD>";

        $oidCopNB=$oidDetail.".1.1";
        $intCopNB=snmpget("$ip", "public", $oidCopNB,$intTimeout);
        echo "<TD>$intCopNB</TD>";

        $oidCopC=$oidDetail.".2.1";
        $intCopC=snmpget("$ip", "public", $oidCopC,$intTimeout);
        echo "<TD>$intCopC</TD>";

        $oidCopM=$oidDetail.".3.1";
        $intCopM=snmpget("$ip", "public", $oidCopM,$intTimeout);
        echo "<TD>$intCopM</TD>";

        $oidCop2c=$oidDetail.".4.1";
        $intCop2c=snmpget("$ip", "public", $oidCop2c,$intTimeout);
        echo "<TD>$intCop2c</TD>";

        //$oid2Couleur=$oidBase.".64";
        //$int2Couleur=snmpget("$ip", "public", $oid2Couleur,$intTimeout);
        //echo "<TD>$int2Couleur</TD>";

        //$oid1Couleur=$oidBase.".80";
        //$int1Couleur=snmpget("$ip", "public", $oid1Couleur,$intTimeout);
        //echo "<TD>$int1Couleur</TD>";


	//$intTotal=$intNB+$intCouleur+$int2Couleur+$int1Couleur;
	//echo "<TD>$intTotal</TD>";

        echo "</TR>";


}

echo "</TABLE>";


?>


            </body>
</html>
